#include "fileutils.h"

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdexcept>
#include <iostream>
#include <fstream>
#include <unordered_map>

int fileSize(const std::string& filename) {
    struct stat status;
    if (::stat(filename.c_str(), &status) == -1) {
        throw std::runtime_error("Couldn't get info about file '" + filename +
                                 "'");
    }
    return status.st_size;
}

bool fileExists(const std::string& name) {
    return std::ifstream(name);
}

std::string getFreeFileName(const std::string& base) {
    static std::unordered_map<std::string, int> count_existing;
    if (count_existing.find(base) == count_existing.end()) {
        if (!fileExists(base)) {
            return base;
        }
        count_existing[base] = 0;
    }
    while (fileExists(base + std::to_string(count_existing[base]))) {
        ++count_existing[base];
    }
    return base + std::to_string(count_existing[base]);
}


template <class Str>
void FileHolder::rename(Str&& new_name) {
    const std::string name(new_name);
    if (std::rename(filename_.c_str(), name.c_str())) {
        throw std::runtime_error("Couldn't rename file '" + filename_ +
                                 "'" + " to '" + name + "'");
    }
    filename_ = name;
}

FileHolder::~FileHolder() {
    if (empty_) {
        return;
    }
    if (std::remove(filename_.c_str())) {
        std::cerr << "Could not remove file " << filename_ << '\n';
    }
}

