#include <iostream>
#include <vector>
#include <string>
#include <stdexcept>
#include <thread>
#include <algorithm>
#include <unordered_map>
#include <boost/filesystem.hpp>
#include <boost/process.hpp>
#include "sort.h"


boost::process::child launch_child(const std::vector<std::string>& args) {
    boost::process::context context;
    context.stdin_behavior = boost::process::capture_stream();
    context.stdout_behavior = boost::process::capture_stream();
    context.stderr_behavior = boost::process::inherit_stream();

    std::vector<std::string> launch_args = args;
    launch_args.erase(launch_args.begin(), launch_args.begin() + 2);
    if (launch_args.empty()) {
        launch_args.emplace_back("");
    }

    return boost::process::launch(args[2], launch_args, context);
}

void stop_child(boost::process::child& child) {
    auto status = child.wait();
    if (!status.exited() || status.exit_status()) {
        throw std::runtime_error("bad child\n");
    }
}

void map(const std::vector<std::string>& args) {
    std::ifstream in_file(args[0]);
    if (in_file.fail()) {
        throw std::runtime_error("Unable to open file " + args[0] + '\n');
    }
    std::ofstream out_file(args[1]);
    for (std::string str; std::getline(in_file, str);) {
        auto child = launch_child(args);

        auto& os = child.get_stdin();
        os << str;
        os.close();

        auto& is = child.get_stdout();
        for (std::string str; std::getline(is, str);) {
            out_file << str << '\n';
        }

        stop_child(child);
    }
}

std::string get_key(const std::string& str) {
    const auto pos = str.find('\t');
    return str.substr(0, pos);
}

void reduce(const std::vector<std::string>& args) {

    FileHolder sorted_file = sort_file(args[0]);
    std::ifstream in_file(sorted_file.getFilename());
    std::ofstream out_file(args[1]);

    std::string previous_key = "\t\t";
    std::string str;
    if (!std::getline(in_file, str)) {
        return;
    }
    int line = 0;
    do {
        auto child = launch_child(args);
        {
            auto& os = child.get_stdin();
            previous_key = get_key(str);
            do {
                std::string key = get_key(str);
                if (key == previous_key) {
                    os << str << '\n';
                } else {
                    previous_key = key;
                    break;
                }
            } while (std::getline(in_file, str));
            os.close();
        }

        std::cerr << "\r\033[K" << ++line;
        {
            std::string str;
            auto& is = child.get_stdout();
            std::getline(is, str);
            out_file << str << '\n';
        }

        stop_child(child);
    } while (!in_file.eof());
    std::cerr << "\r\033[K";
}


int main(int argc, char* argv[]) {
    struct ArgsError : std::exception {};
    try {
        std::vector<std::string> args;
        for (int i = 0; i < argc; ++i) {
            args.emplace_back(argv[i]);
        }
        if (args.size() < 2) {
            throw ArgsError();
        }
        if (args[1] == "map") {
            args.erase(args.begin(), args.begin() + 2);
            if (args.size() < 3) {
                throw ArgsError();
            }
            map(args);
        } else if (args[1] == "reduce") {
            args.erase(args.begin(), args.begin() + 2);
            if (args.size() < 3) {
                throw ArgsError();
            }
            reduce(args);
        } else if (args[1] == "sort") {
            args.erase(args.begin(), args.begin() + 2);
            if (args.size() < 1) {
                throw ArgsError();
            }
            std::cerr << "sorting file " << args[0] << "...\n";
            auto sorted = sort_file(args[0]);
            sorted.drop();
            std::cerr << "output file: " << sorted.getFilename() << "\n";
        } else {
            throw ArgsError();
        }
    } catch (ArgsError ex) {
        std::cout <<
"Usage: mapreduce map <src_file> <dst_file> <path_to_map_script> [arguments] |\n\
       mapreduce reduce <src_file> <dst_file> <path_to_reduce_script> [arguments] |\n\
       mapreduce sort <file>\n";
    } catch (std::runtime_error ex) {
        std::cerr << "Error occured. what():\n" << ex.what();
    }
    return 0;
}

