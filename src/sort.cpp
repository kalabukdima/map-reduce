#include "sort.h"
#include <queue>
#include <cstdio>
#include <fstream>
#include <sstream>
#include <algorithm>

// number of lines that can be load in memory
constexpr int kMaxLinesInMemory = 1024;

// Divides given file in chunks of kMaxLinexInMemory sorted lines
std::vector<FileHolder> divide_by_chunks(const std::string& filename) {
    std::ifstream file(filename);
    if (file.fail()) {
        throw std::runtime_error("Unable to open file " + filename + '\n');
    }

    std::vector<FileHolder> chunk_files;
    for (int chunk_index = 0; !file.eof(); ++chunk_index) {
        std::vector<std::string> chunk;
        chunk.reserve(kMaxLinesInMemory);
        for (int i = 0; i < kMaxLinesInMemory && !file.eof(); ++i) {
            std::string str;
            if (!std::getline(file, str)) {
                break;
            }
            chunk.emplace_back(str);
        }
        if (chunk.empty()) {
            break;
        }
        std::sort(chunk.begin(), chunk.end());

        chunk_files.emplace_back(getFreeFileName(".chunk"));

        std::ofstream out_file(chunk_files.back().getFilename());
        for (const auto& line : chunk) {
            out_file << line << '\n';
        }
    }
    return chunk_files;
}

FileHolder sort_file(const std::string& filename) {
    const auto chunk_files = divide_by_chunks(filename);
    const int N = chunk_files.size();
    const int kBufferMaxSize = std::max(kMaxLinesInMemory / std::max(N, 1), 1);

    // open chunks to read from
    std::deque<std::ifstream> chunk_streams;
    for (const auto& file : chunk_files) {
        chunk_streams.emplace_back(file.getFilename());
        if (chunk_streams.back().fail()) {
            throw std::runtime_error("Unable to open file " + file.getFilename() + '\n');
        }
    }

    // initialize buffers
    std::vector<std::queue<std::string>> buffers(N);
    for (int i = 0; i < N; ++i) {
        int j = 0;
        for (std::string str; j < kBufferMaxSize && std::getline(chunk_streams[i], str); ++j) {
            buffers[i].emplace(str);
        }
    }

    // merge all chunks together
    FileHolder output_file("." + filename + "_sorted");
    std::ofstream out_file(output_file.getFilename());
    while (true) {
        int smallest_index = 0;
        for (int i = 1; i < N; ++i) {
            if (buffers[i].empty()) {
                continue;
            }
            if (buffers[smallest_index].empty()
                || buffers[smallest_index].front() > buffers[i].front()) {
                smallest_index = i;
            }
        }
        if (buffers[smallest_index].empty()) {
            break;
        }
        out_file << buffers[smallest_index].front() << '\n';
        buffers[smallest_index].pop();
        if (buffers[smallest_index].empty() && !chunk_streams[smallest_index].eof()) {
            int j = 0;
            for (std::string str; j < kBufferMaxSize && std::getline(chunk_streams[smallest_index], str); ++j) {
                buffers[smallest_index].emplace(str);
            }
        }
    }

    return output_file;
}

