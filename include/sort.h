#pragma once
#include <stdexcept>
#include <vector>
#include <string>
#include "fileutils.h"

FileHolder sort_file(const std::string& filename);

void remove_files(const std::vector<std::string>& names);

