#pragma once

#include <string>
#include <fstream>

int fileSize(const std::string& filename);

bool fileExists(const std::string& filename);

// Returns base if file with name base doesn't exist in current directory.
// Otherwise returns base + '1' if it doesn't exist, otherwise base + '2'
// and so on.
std::string getFreeFileName(const std::string& base);

// Removes file on destruction
class FileHolder {
public:
    FileHolder(const std::string& filename) : filename_(filename) {
        std::ofstream(filename_); // Touch file
    }

    FileHolder(std::string&& base) : FileHolder(base) { }

    FileHolder(const FileHolder&) = delete;

    FileHolder(FileHolder&& rhs) {
        filename_ = std::move(rhs.filename_);
        rhs.empty_ = true;
    }

    ~FileHolder();

    const std::string& getFilename() const noexcept {
        return filename_;
    }

    // Cancels removal
    void drop() {
        empty_ = true;
    }

    template <class Str>
    void rename(Str&& new_name);

private:
    std::string filename_;
    bool empty_ = false;
};

