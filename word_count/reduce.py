#! /usr/bin/python
"""Counts number of entries in stdin"""
import sys

# ensure that script receives arguments
if len(sys.argv) > 1:
    sys.stderr.write("Called reduce with arguments " + ', '.join(sys.argv[1:]) + '\n')

count = 0
string = ""
for line in sys.stdin:
    key, value = line.split('\t')
    string = key
    count += int(value)
print(string, '\t', count, sep='')

