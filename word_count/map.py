#! /usr/bin/python
"""Assigns each key value 1"""
import sys

# ensure that script receives arguments
if len(sys.argv) > 1:
    sys.stderr.write("Called map with arguments " + ', '.join(sys.argv[1:]) + '\n')

line = input()
_, value = line.split('\t')
words = value.split(' ')

counts = {}
for word in words:
    if not word in counts:
        counts[word] = 0
    counts[word] += 1

for word, count in counts.items():
    print(word, '\t',count, sep='')

