# MapReduce

Реализация метода модели вычислений MapReduce.
В данный момент однопоточная.

## Компиляция
Dependencies:
* [boost/process](http://www.highscore.de/boost/process/index.html)

В корневом каталоге запустить
```
make
```

## Использование
```
./mapreduce map <input_file> <output_file> <map_script> [arguments]
./mapreduce reduce <intput_file> <output_file> <reduce_script> [arguments]
./mapreduce sort <file>
```
`input_file` и `output_file` - файлы в формате tsv, где каждая строка содержит
ключ и значение

`map_script` должен читать из `stdin` пару ключ/значение и выводить в `stdout`
список пар ключ/значение в формате tsv.

`reduce_script` читает из `stdin` набор пар с одинаковым ключом и выводит в
`stdout` список пар в формате tsv.

## Пример: подсчёт частоты встречаемости слов
Необходимые скрипты находятся в папке `word_count`
```
./mapreduce map word_count/input word_count/tmp word_count/map.py
./mapreduce reduce word_count/tmp word_count/output word_count/reduce.py
rm word_count/tmp
```
