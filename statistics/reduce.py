#!/usr/bin/python
import sys


def main() :
    sum_of_numbers = 0
    sum_of_squares = 0
    number_of_lines = 0
    string = ""
    for line in sys.stdin:
        key, value = line.split('\t')
        string = key
        value = float(value)
        sum_of_numbers += value
        sum_of_squares += value ** 2
        number_of_lines += 1

    mean = sum_of_numbers / number_of_lines
    variance = sum_of_squares / number_of_lines - mean ** 2
    print(string, '\t', mean, ' ', variance, sep='')

if __name__ == '__main__':
    main()
