#!/usr/bin/python

import random
import argparse


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('-n', '--lines', help='number of lines in output file',
                        type=int, metavar='lines')
    parser.add_argument('-N', '--limit', help='generate keys in [0, n)',
                        type=int, metavar='n')
    parser.add_argument('-d', '--output', help='output file name', type=str,
                        metavar='file')
    args = parser.parse_args()

    with open(args.output, 'w') as file:
        for i in range(args.lines):
            key = random.randrange(0, args.limit)
            value = random.normalvariate(0, 1)
            file.write(str(key) + '\t' + str(value) + '\n')

if __name__ == '__main__':
    main()
