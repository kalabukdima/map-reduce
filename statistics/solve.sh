#!/bin/sh

echo Generating input
./generate.py -n 65536 -N 5 -d input
echo Computing
../mapreduce reduce input output ./reduce.py
echo Done!
cat output
