SRCS = $(wildcard src/*.cpp)
APPNAME = mapreduce
HEADERS = -I "include/" -I "/usr/include/boost"
OBJS = $(patsubst src/%.cpp, obj/%.o, $(SRCS))
OBJDIRS = $(sort $(dir $(OBJS)) )

CXX = g++
LD = g++
DEBUG = -Og
WARNINGS = -Wall -Wextra -Wno-unused-parameter
SANITIZERS = -fsanitize=undefined,address
CXXFLAGS = -std=c++14 -pedantic $(DEBUG) $(SANITIZERS) $(WARNINGS)
LDLIBS = -lboost_system
LDFLAGS = $(LDLIBS) $(SANITIZERS)

#######################################################################################################################

all: $(OBJDIRS) $(APPNAME)

$(APPNAME): $(OBJS)
	$(LD) $(OBJS) -o $@ $(LDFLAGS) $(HEADERS)
	@sed -i -z -e 's/\\\n/#/g' .depend
	@sort -u .depend -o .depend
	@sed -i -z -e 's/#/\\\n/g' .depend

obj/%.o: src/%.cpp
	$(CXX) -c $< -o $@ $(CXXFLAGS) $(HEADERS)
	$(CXX) -MM $< -MT $@ $(CXXFLAGS) $(HEADERS) >> ./.depend

#######################################################################################################################

$(OBJDIRS):
	mkdir $(OBJDIRS) -p

.depend:
	touch .depend

include .depend

.PHONY: clean run echo

clean:
	rm -rf obj/ .depend

